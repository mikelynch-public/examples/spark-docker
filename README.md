# spark-docker

Simple project to run a 4 node Spark cluster deployed within Docker containers. The Spark cluster is comprised of 1 master node and 3 worker nodes. A spark-base container is used as the foundation container that the spark-master and spark-worker nodes use as the base container.

#### Build
Each of the Spark components (spark-base, spark-master, spark-worker) are in a sub-directory under the project root. Each docker image must be build from the Dockerfile for each Spark component.

```
$ docker build -f spark-base/Dockerfile -t spark-base:0.0.1 .
$ docker build -f spark-master/Dockerfile -t spark-master:0.0.1 .
$ docker build -f spark-worker/Dockerfile -t spark-worker:0.0.1 .
```

Once the continers are built. Verify that they have been created correctly:

```
$ docker images
REPOSITORY                                         TAG                 IMAGE ID            CREATED             SIZE
spark-worker                                       0.0.1               b3b08bdeb2ab        3 seconds ago       862MB
spark-master                                       0.0.1               fbb6935aca15        50 seconds ago      862MB
spark-base                                         0.0.1               fee8280a50bc        6 minutes ago       862MB
```

Configure the Docker network:

```
$ docker network create spark-network
```

#### Run
 ```
$ docker run --name spark-master --hostname spark-master --network spark-network -p 8080:8080 --detach -it spark-master:0.0.1
$ docker run --name spark-worker-1 --network spark-network --hostname spark-worker-1 -p 8081:8081 --detach -it spark-worker:0.0.1
 ```
 
Verify that the master and worker nodes are running:
[Spark](http://localhost:8080/)
 

#### Run using Docker Compose
 ```
$ docker-compose up
 ```
 
Verify that the master and worker nodes are running:
[Spark](http://localhost:8080/)

#### Run using Docker Compose in Detached Mode
 ```
$ docker-compose up -d
 ```
Verify that the master and worker nodes are running:
[Spark](http://localhost:8080/)

View logs

 ```
$ docker-compose logs
$ docker-compose logs spark-worker-1
 ```
 
#### Docker Command Reference 
```
$ docker images #display docker images
$ docker ps #display running container
$ docker ps -a #display all containers
$ docker rm -f $(docker ps -a -q) #remove all containers
```
 






 



